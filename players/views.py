from django.shortcuts import render
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_view(request):
    from utils.utils import scrapper

    scrapper = scrapper()

    context = {
        'scrapper': scrapper
    }
    return render(request, 'players/list_players.html', context)
