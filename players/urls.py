from django.urls import path

from players.views import list_view
app_name = 'players'

urlpatterns = [
     path('list/', list_view, name='list')
 ]
