from django.test import TestCase
from django.urls import reverse_lazy


class PlayerListView(TestCase):

    def test_player_list_is_empty(self):
        url = reverse_lazy('players:list')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        self.assertGreater(len(res.context['scrapper']), 0)



