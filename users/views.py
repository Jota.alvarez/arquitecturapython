from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from users.forms import RegisterFormUser


def register_view(request):
    if request.method == "POST":
        form = RegisterFormUser(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            #messages.success(request, f"Account created {username}!")
            return redirect("/")
        else:
            context = {"form": form}
            return render(request, 'users/register.html', context)
    else:
        form = RegisterFormUser()
    context = {"form": form}
    return render(request, 'users/register.html', context)


def login_view(request):
    if request.method == 'GET':
        return render(request, 'users/login.html', {})
    elif request.method == 'POST':
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect("/players/list")
        else:
            messages.error(request, 'User or Password incorrect')
            return render(request, 'users/login.html', {})


"""class LogoutView(RedirectView):
    url = reverse_lazy('users:login')

    def get_redirect_url(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, kwargs)"""


# logout function

def logout_view(request):
    logout(request)
    return redirect('/users/login/')

