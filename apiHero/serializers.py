from rest_framework import serializers

from .models import Hero, City


class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hero
        fields = ('name', 'alias', 'urlImg', 'city')


class CitySerializer(serializers.HyperlinkedModelSerializer):
    heroes = serializers.StringRelatedField(many=True)
    class Meta:
        model = City
        fields = ('nameCity', 'Country', 'heroes')
