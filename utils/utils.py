import requests
from bs4 import BeautifulSoup


def scrapper():

    url = 'https://www.lanacion.com.ar/deportes/futbol/ranking-mbappe-messi-ronaldo-nid2613021/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    title = soup.title.string
    data = []
    player = soup.find('ul', 'com-ordered')
    for i in player:
        data.append(i.text)

    context = {
        'data': data,
        'title': title,
    }
    return context
